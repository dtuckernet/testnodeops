var express = require("express");
var app = express();
app.use(express.logger());

app.get('*', function(request, response) {
  response.send(JSON.stringify(process.env));
});

var port = 80;
app.listen(port, function() {
  console.log("Listening on " + port);
});
